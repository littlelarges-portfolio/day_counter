import 'package:day_counter/logic/data/app_data.dart';
import 'package:day_counter/logic/preferens/app_preferenes.dart';
import 'package:flutter/material.dart';

import 'logic/routes/app_router.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await AppPreferences.init();

  appData.dates = AppPreferences.loadData();

  runApp(const MyApp());
}

final AppRouter router = AppRouter();

final AppData appData = AppData();

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.green.shade400),
        useMaterial3: true,
      ),
      routerConfig: router.router,
    );
  }
}
