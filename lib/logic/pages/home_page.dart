import 'dart:async';

import 'package:day_counter/logic/events/app_events.dart';
import 'package:day_counter/logic/preferens/app_preferenes.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:flutter_svg/svg.dart';
import 'package:go_router/go_router.dart';

import '../../main.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key, required this.title});

  final String title;

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  void initState() {
    super.initState();

    AppEvents.onDateAdd.stream.listen((event) {
      if (event) {
        setState(() {});
      }
    });

    AppEvents.onDateDelete.stream.listen((event) {
      if (event) {
        setState(() {});
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Center(child: Text(widget.title)),
      ),
      body: Padding(
        padding: const EdgeInsets.only(right: 20, left: 20),
        child: appData.dates.isNotEmpty
            ? const Column(
                children: [
                  TableTitle(),
                  DataList(),
                ],
              )
            : const NoData(),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          context.push('/add');
        },
        tooltip: 'Add',
        child: const Icon(Icons.add),
      ),
    );
  }
}

class TableTitle extends StatelessWidget {
  const TableTitle({super.key});

  @override
  Widget build(BuildContext context) {
    return Table(
      columnWidths: const {
        // 0: FlexColumnWidth(8),
        // 1: FlexColumnWidth(2),
      },
      children: [
        TableRow(children: [
          TableCell(
              child: Container(
            child: const Padding(
              padding: EdgeInsets.all(8.0),
              child: Center(
                  child: Text(
                'Date name',
              )),
            ),
          )),
          TableCell(
              child: Container(
            child: const Padding(
              padding: EdgeInsets.all(8.0),
              child: Center(
                  child: Text(
                'Date',
              )),
            ),
          )),
          TableCell(
              child: Container(
            child: const Padding(
              padding: EdgeInsets.all(8.0),
              child: Center(
                  child: Text(
                'Days',
              )),
            ),
          )),
        ]),
      ],
    );
  }
}

class TableContent extends StatelessWidget {
  TableContent(this.dateName, this.date, this.days, this.isEven, {super.key});

  String dateName;
  DateTime date;
  int days;

  bool isEven;

  @override
  Widget build(BuildContext context) {
    return Table(
      columnWidths: const {
        // 0: FlexColumnWidth(8),
        // 1: FlexColumnWidth(2),
      },
      children: [
        TableRow(children: [
          TableCell(
              verticalAlignment: TableCellVerticalAlignment.middle,
              child: Container(
                color: getCellColor(isEven),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Center(
                      child: Text(
                    dateName,
                  )),
                ),
              )),
          TableCell(
              verticalAlignment: TableCellVerticalAlignment.fill,
              child: Container(
                color: getCellColor(isEven),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Center(
                      child: Text(
                    '${date.year}-${date.month}-${date.day}',
                  )),
                ),
              )),
          TableCell(
              verticalAlignment: TableCellVerticalAlignment.fill,
              child: Container(
                color: getCellColor(isEven),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Center(
                      child: Text(
                    '$days',
                  )),
                ),
              )),
        ]),
      ],
    );
  }

  Color? getCellColor(bool isEven) => isEven ? Colors.green[50] : Colors.green[100];
}

class NoData extends StatelessWidget {
  const NoData({super.key});

  @override
  Widget build(BuildContext context) {
    return Center(
        child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Text('Nothing yet...', style: Theme.of(context).textTheme.bodyLarge),
        Text('Tap \'+\' button to add',
            style: Theme.of(context).textTheme.bodyLarge)
      ],
    ));
  }
}

class DataList extends StatefulWidget {
  const DataList({super.key});

  @override
  State<DataList> createState() => _DataListState();
}

class _DataListState extends State<DataList> {
  late final StreamSubscription<bool> onDateAddSubscription;
  late final StreamSubscription<bool> onDateDeleteSubscription;

  @override
  void initState() {
    super.initState();

    onDateAddSubscription = AppEvents.onDateAdd.stream.listen((event) {
      if (event) {
        setState(() {});
      }
    });

    onDateDeleteSubscription = AppEvents.onDateDelete.stream.listen((event) {
      if (event) {
        setState(() {});
      }
    });
  }

  @override
  void dispose() {
    super.dispose();

    onDateAddSubscription.cancel();
    onDateDeleteSubscription.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
        child: ListView.builder(
            itemCount: appData.dates.length,
            itemBuilder: (context, index) {
              return Slidable(
                  endActionPane:
                      ActionPane(motion: const StretchMotion(), children: [
                    SlidableAction(
                      onPressed: (context) {
                        appData.dates.removeAt(index);

                        AppPreferences.saveData(appData.dates);

                        AppEvents.onDateDelete.add(true);
                      },
                      backgroundColor: Colors.redAccent,
                      foregroundColor: Colors.white,
                      icon: Icons.delete_outline_rounded,
                    )
                  ]),
                  child: TableContent(
                      appData.dates[index].dateName,
                      appData.dates[index].date,
                      appData.dates[index].daysUntil,
                      index.isEven));
            }));
  }
}
