import 'package:day_counter/logic/data/app_data.dart';
import 'package:day_counter/logic/events/app_events.dart';
import 'package:day_counter/logic/preferens/app_preferenes.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';

import '../../main.dart';

class AddPage extends StatefulWidget {
  const AddPage({Key? key}) : super(key: key);

  @override
  _AddPageState createState() => _AddPageState();
}

class _AddPageState extends State<AddPage> {
  late DateTime selectedDate;

  String dateName = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Add Page'),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(right: 20, left: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(height: 30),
              Text('Date name:',
                  style: Theme.of(context).textTheme.titleLarge),
              TextField(maxLength: 36, onChanged: _saveFromInput),
              const SizedBox(height: 30),
              Text('Select date:',
                  style: Theme.of(context).textTheme.titleLarge),
              const SizedBox(height: 30),
              SfDateRangePicker(
                onSelectionChanged: (dateRangePickerSelectionChangedArgs) {
                  selectedDate = dateRangePickerSelectionChangedArgs.value;

                  print(selectedDate);
                },
              ),
              Center(
                child: ElevatedButton(
                    onPressed: () {
                      try {
                        int hoursDifference =
                            selectedDate.difference(DateTime.now()).inHours;

                        if (!hoursDifference.isNegative) {
                          print(hoursDifference);

                          int days = (hoursDifference / 24).ceil();

                          print(days);

                          appData.dates.add(DateInfo(
                              dateName: dateName,
                              date: selectedDate,
                              daysUntil: days));

                          context.pop();

                          AppPreferences.saveData(appData.dates);

                          AppEvents.onDateAdd.add(true);
                        }
                      } catch (e) {
                        ScaffoldMessenger.of(context)
                            .showSnackBar(const SnackBar(
                          content: Text('Error! Select date'),
                          backgroundColor: Colors.red,
                        ));
                      }
                    },
                    child: const Text(
                      'Select',
                    )),
              )
            ],
          ),
        ),
      ),
    );
  }

  void _saveFromInput(String value) {
    dateName = value;
  }
}
