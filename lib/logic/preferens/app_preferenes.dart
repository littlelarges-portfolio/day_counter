import 'package:day_counter/logic/data/app_data.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AppPreferences {
  static late final SharedPreferences _prefs;

  static List<String> _keys = [];

  static const String _keysListKey = 'KEYS_LIST';

  static Future init() async {
    _prefs = await SharedPreferences.getInstance();
  }

  static List<DateInfo> loadData() {
    _keys = _prefs.getStringList(_keysListKey) ?? List.empty();

    List<DateInfo> dates = [];

    for (int i = 0; i < _keys.length; i++) {
      String dateName = _prefs.getString('$i-name') ?? '';
      DateTime date = DateTime.parse(_prefs.getString('$i-date') ?? '');
      int daysUntil = _prefs.getInt('$i-daysUntil') ?? 0;

      dates.add(DateInfo(dateName: dateName, date: date, daysUntil: daysUntil));
    }

    return dates;
  }

  static void saveData(List<DateInfo> dates) {
    for (int i = 0; i < dates.length; i++) {
      _prefs.setString('$i-name', dates[i].dateName);
      _prefs.setString('$i-date',
          '${dates[i].date.year}-${dates[i].date.month}-${dates[i].date.day}');
      _prefs.setInt('$i-daysUntil', dates[i].daysUntil);
    }

    _keys = List.generate(dates.length, (index) => index.toString());

    _prefs.setStringList(_keysListKey, _keys);
  }
}

class DateData {
  DateData({required this.name, required this.date, required this.daysUntil});

  String name;
  DateTime date;
  int daysUntil;
}
