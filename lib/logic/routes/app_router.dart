import 'package:day_counter/logic/pages/home_page.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

import '../pages/add_page.dart';

class AppRouter {
  final GoRouter router = GoRouter(routes: [
    GoRoute(
      path: '/add',
      pageBuilder: (context, state) {
        return CustomTransitionPage(
            child: const AddPage(),
            transitionsBuilder: (BuildContext context,
                Animation<double> animation,
                Animation<double> secondaryAnimation,
                Widget child) {
              return const AddPage();
            },
            barrierColor: Colors.transparent);
      },
    ),
    GoRoute(
      path: '/',
      builder: (context, state) {
        return const HomePage(title: 'Day Counter',);
      },
    ),
  ]);
}
