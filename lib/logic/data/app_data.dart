class AppData {
  List<DateInfo> dates = [
  ];
}

class DateInfo {
  DateInfo(
      {required this.dateName, required this.date, required this.daysUntil});

  String dateName;
  DateTime date;
  int daysUntil;
}
