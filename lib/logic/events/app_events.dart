import 'dart:async';

class AppEvents {
  static final StreamController<bool> onDateAdd = StreamController<bool>.broadcast();
  static final StreamController<bool> onDateDelete = StreamController<bool>.broadcast();
}