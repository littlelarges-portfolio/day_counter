# Day counter app

~7-8 hours were spent on development

<img src="https://i.imgur.com/VKH1AlX.png" width="188" height="354">

To add a new date:

1. Click on the '+' button
2. Give the date a name, select the date and click on the 'select' button to add a new date
3. Swipe left on a date that is no longer needed and click on the delete button to delete it

<img src="https://i.imgur.com/gemAlJM.png" width="188" height="354"> <img src="https://i.imgur.com/APqDJf9.png" width="188" height="354"> <img src="https://i.imgur.com/COwOy7t.png" width="188" height="354">

Demo:

<img src="demo/day_counter_demo.gif" width="188" height="354">